from PIL import Image

import zipfile
import os

print("default resolution is set to 'fullscreen' windowed mode(1920x1017)")
print("if your screenshot resolution is different check the README for instructions.")


alpha = int(input("\nplease enter overlay alpha transparency(0 - 255):"))

print("\nplease drag in the source screenshot into this folder.")
source_path = input("what is the name of the source image?: ")

img = Image.open(source_path)
img.putalpha(alpha)
img.save("assets/minecraft/textures/effect/image.png")




# save resource pack as zip

with zipfile.ZipFile('overlay_resourcepack.zip', 'w') as archive:

    archive.write('pack.png')
    archive.write('pack.mcmeta')
    
    # Add a directory to the archive
    for foldername, subfolders, filenames in os.walk('assets'):
        for filename in filenames:
            filepath = os.path.join(foldername, filename)
            archive.write(filepath)



print("\n\ndone")

input()
