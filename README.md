info:
============= 
this resource pack helps you align the perspective from a screenshot
default resolution is set to 'fullscreen' windowed mode(1920x1017), and fullscreen is normally 1980x1080. to match the perspectives nicely make sure the source and target aspect ratios are the same, if the screenshot was taken in fullscreen then match it in full screen and viceversa


how to use:
============= 
run start.bat (or start.py if it doesnt close immediately before you can write anything)
after its done drag overlay_resourcepack.zip into your resource pack folder in .minecraft

doesnt work?
============= 
use any image editing software to change the opacity and save it in textures/effect/image.png
then compress normally and drag it into your resource pack folder.

important:
============= 
the screenshot overlay resolution and the game resolution must match. if they dont you wont be able to align it properly or it will crash.
the video settings must be set to FABULOUS for the overlay to render.
made for minecraft 1.16.5

credits:
============= 
https://github.com/onnowhere/VertexEdit
